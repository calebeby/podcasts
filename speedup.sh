#!/bin/bash
# Make script crontab friendly:
cd $(dirname $0)

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

killed=false
function ctrl_c() {
  killed=true
  echo
  rm fast/*/.* 2>/dev/null 
  echo Removed incomplete podcasts
  echo Bye!
  exit 1
}

#read lines, not words in string
IFS=$'\n'

echo Welcome to the FANTASTIC Podcast Speeder-Upper!
for podcast in $(find normal/ -name '*.mp3' | sort -V); do
  channel=$(echo $podcast | cut -d '/' -f 2)
  filename=$(echo $podcast | cut -d '/' -f 3)
  mkdir -p fast
  mkdir -p fast/$channel
  if [ ! -f fast/$channel/$filename ] ; then
    while [ $(jobs | wc -l) -ge 2 ]; do
      jobs=$(jobs | wc -l)
      sleep 2
    done
    if [ "$killed" = false ]; then
      echo "☐ $filename by $channel"
      touch fast/$channel/.$filename
      ffmpeg -nostats -hide_banner -loglevel panic -y -i normal/$channel/$filename -filter:a "atempo=1.5" -c:v copy fast/$channel/.$filename \
      && mv "fast/$channel/.$filename" "fast/$channel/$filename" \
      && echo "☑ $filename by $channel" \
      && notify-send "Finished speeding up" "Finished speeding up $filename by $channel" &
    fi
  fi
done
wait
echo Bye!
