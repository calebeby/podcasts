#!/bin/bash
# Make script crontab friendly:
cd $(dirname $0)

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

error() {
  echo -e "\e[1m\e[31m$1\e[0m"
}

exit_with() {
  error "$1; Exiting"
  leave
  exit 1
}

dashes() {
  echo -----------------------------------
}

leave() {
  cd ~
  echo Syncing files
  sync
  sudo umount /dev/sdb
  wait
  echo Bye!
}

function ctrl_c() {
  leave
  exit 1
}
#read lines, not words in string
#Internal Field Separator
IFS=$'\n'

echo Welcome to the FANTASTIC Podcast transferer!
sudo echo

# Mount the device
pmount /dev/sdb || exit_with 'Please attatch the device to /dev/sdb'

# Go to the device
cd /media/sdb/Podcasts || exit_with "Can't find the Podcasts folder"

for channel in $(ls * -dv); do
  echo Checking $channel
  dashes

  # Go to the channel
  cd $channel || error "Error checking channel"

  # check for existing files
  current_files="$(ls -v)"

  # if there aren't existing files
  if [ -z "$current_files" ]; then
    error "No files found for $channel"
  # there are existing files
  else

    echo "$current_files"
    dashes

    most_recent="$(echo "$current_files" | tail -n 1)"

    directory=$(dirname $(find "$HOME/Podcasts/fast/" -name $most_recent | tail -n 1))

    # If the files are not found localy
    if [ -z "$directory" ]; then
      error "Can't find podcast $channel"

    # found the local podcasts
    else

      echo $directory
      dashes

      file_list="$(ls -v $directory)"

      # get the next episode
      #              get current files   get from most recent onward  remove most recent
      #                     |                          |                  |     get first line 
      #                     V                          V                  V           V
      newer_files="$(echo "$file_list" | grep -A100000 $most_recent | tail -n +2 | head -n 1)"

      echo "$newer_files"
      dashes

      # if there aren't newer episodes
      if [ -z "$newer_files" ]; then
        echo "No newer episodes found for $channel"
      else
        # Remove existing file(s)
        for file in $current_files; do
          # echo "Removing $file"
          rm $file
          # rm --verbose $file
        done

        # Copy new file(s)
        for file in $newer_files; do
          path="$directory/$file"
          # echo "Copying $path -> $file"
          cp $path .
          # cp --verbose $path .
        done
      fi
    fi
    # reset
    cd ..
    echo
  fi
done

leave
