#!/bin/bash
# Make script crontab friendly:
cd $(dirname $0)

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

error() {
  echo -e "\e[1m\e[31m$1\e[0m"
}

exit_with() {
  error "$1; Exiting"
  leave
  exit 1
}

dashes() {
  echo -----------------------------------
}

leave() {
  cd ~
  echo Syncing files
  sync
  sudo umount /dev/sdb
  wait
  echo Bye!
}

function ctrl_c() {
  leave
  exit 1
}
#read lines, not words in string
#Internal Field Separator
IFS=$'\n'

echo Welcome to the FANTASTIC Podcast re-transferer!
sudo echo

# Mount the device
pmount /dev/sdb || exit_with 'Please attatch the device to /dev/sdb'

# Go to the device
cd /media/sdb/Podcasts || exit_with "Can't find the Podcasts folder"

for channel in $(ls * -dv); do
  echo Checking $channel
  dashes

  # Go to the channel
  cd $channel || error "Error checking channel"

  # check for existing files
  current_files="$(ls -v)"

  # if there aren't existing files
  if [ -z "$current_files" ]; then
    error "No files found for $channel"
  # there are existing files
  else

    echo "$current_files"
    dashes

    most_recent="$(echo "$current_files" | tail -n 1)"

    local_file=$(find "$HOME/Podcasts/fast/" -name $most_recent | tail -n 1)

    # If the files are not found localy
    if [ -z "$local_file" ]; then
      error "Can't find podcast $channel"

    # found the local podcasts
    else

      echo $local_file
      dashes

      cp --verbose $local_file .
    fi
    # reset
    cd ..
    echo
  fi
done

leave
