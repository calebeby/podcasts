package main

import (
	"fmt"
	"github.com/briandowns/spinner"
	"github.com/cavaliercoder/grab"
	"github.com/mmcdole/gofeed"
	"os"
	"os/signal"
	"os/user"
	"regexp"
	"sync"
	"syscall"
	"time"
)

var spinners = []string{"⡏ ", "⠏ ", "⠏⠁", "⠋⠁", "⠋⠉", "⠉⠉", "⠉⠙", "⠈⠙", "⠈⠹", " ⢹", " ⢸", " ⣸", " ⣰", "⢀⣰", "⢀⣠", "⣀⣠", "⣀⣀", "⣆⡀", "⣆ ", "⣇ ", "⡇ "}
var s = spinner.New(spinners, 50*time.Millisecond)

var tmp_dir = ".tmp_normal/"
var out_dir = "normal/"
var fast_dir = "fast/"

func print(s string) {
	fmt.Print(s + "\n")
}

func download(filename string, URL string, channel chan string, wg *sync.WaitGroup) {
	print("  " + filename)
	grab.Get(tmp_dir+filename, URL)
	os.Rename(tmp_dir+filename, out_dir+filename)
	wg.Done()
	print("✔ " + filename)
	channel <- filename
}

func cleanup() {
	s.Stop()
	usr, _ := user.Current()
	homedir := usr.HomeDir
	podcastdir := homedir + "/Podcasts"
	os.Chdir(podcastdir)
	os.RemoveAll(tmp_dir)
}

func mkdirp(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.MkdirAll(path, 0700)
	}
}

func main() {
	s.Color("yellow")
	s.FinalMSG = "hi\033[2K"
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		cleanup()
		os.Exit(1)
	}()
	usr, _ := user.Current()
	homedir := usr.HomeDir
	podcastdir := homedir + "/Podcasts"
	os.Chdir(podcastdir)

	var wg sync.WaitGroup

	var feeds = []string{
		"http://feeds.feedburner.com/ThePiPodcast",
		"http://feeds.soundcloud.com/users/soundcloud:users:174310699/sounds.rss",
		"http://feeds.soundcloud.com/users/soundcloud:users:54298479/sounds.rss",
		"http://feeds.soundcloud.com/users/soundcloud:users:59736920/sounds.rss",
		"http://frcgamesense.com/GSPodcast.xml",
		"https://blog.codepen.io/feed/podcast/",
		"http://artofproductpodcast.com/rss",
		"https://feed.syntax.fm/rss",
		"https://changelog.com/founderstalk/feed",
		"https://changelog.com/gotime/feed",
		"https://changelog.com/jsparty/feed",
		"https://changelog.com/podcast/feed",
		"https://changelog.com/rfc/feed",
		"https://changelog.com/spotlight/feed",
		"https://simplecast.com/podcasts/1088/rss",
		"https://simplecast.com/podcasts/271/rss",
		"https://simplecast.com/podcasts/282/rss",
		"https://stackoverflow.blog/feed/podcast/",
		"https://www.briefs.fm/3-minutes-with-kent.xml",
		"https://www.briefs.fm/git-tips.xml",
		"https://www.briefs.fm/vim-tips-with-ben.xml",
		"https://www.heavybit.com/category/library/podcasts/jamstack-radio/feed/",
	}
	s.Start()
	s.Reverse()
	s.Restart()
	s.Suffix = " Generating file list..."
	fp := gofeed.NewParser()
	reg := regexp.MustCompile("[^A-Za-z0-9]+")

	urls := make([]string, 0)
	files := make([]string, 0)

	for _, url := range feeds {
		wg.Add(1)
		go func(url string) {
			feed, err := fp.ParseURL(url)

			if err != nil {
				panic(err)
			}

			podcast := reg.ReplaceAllString(feed.Title, "-")

			folder := podcast
			mkdirp(out_dir + folder)
			mkdirp(tmp_dir + folder)

			for _, item := range feed.Items {
				title := reg.ReplaceAllString(item.Title, "-")
				date := item.PublishedParsed.Format("2006-01-02")
				newpath := folder + "/" + date + "-" + title + ".mp3"
				enclosure := item.Enclosures[0]

				if _, err := os.Stat(out_dir + newpath); os.IsNotExist(err) {
					url := enclosure.URL
					urls = append(urls, url)
					files = append(files, newpath)
				}
			}
			wg.Done()
		}(url)
	}

	wg.Wait()
	s.Stop()
	s.Reverse()
	s.Restart()
	s.Suffix = " Downloading"
	s.Start()
	s.Color("blue")
	currentDownloads := 0
	channel := make(chan string)
	for i, _ := range files {
		file := files[i]
		url := urls[i]
		wg.Add(1)
		currentDownloads++
		go download(file, url, channel, &wg)
		if currentDownloads >= 5 {
			<-channel
			currentDownloads--
		}
	}
	wg.Wait()
	cleanup()
	s.Suffix = ""
}
